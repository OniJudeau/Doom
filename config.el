;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Oleksii Kapula"
      user-mail-address "")

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!
(let ((font "JetBrains Mono"))
  (setq doom-font (font-spec :family font :size 18)
        doom-big-font (font-spec :family font :size 24)))

(setq doom-serif-font (font-spec :family "Noto Serif" :size 18)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 18)
      doom-symbol-font (font-spec :family "JetBrainsMono Nerd Font"))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

(add-hook! '(conf-mode-mode prog-mode-hook)
  (setq-local display-line-numbers-type 'relative))

(setq mouse-wheel-scroll-amount '(2 ((shift) . hscroll))
      mouse-wheel-progressive-speed nil
      mouse-wheel-follow-mouse t)

(customize-set-variable 'mouse-avoidance-mode 'banish)

(setq scroll-margin 9)

(defvar my/frame-geometry-file
  (expand-file-name "frame-geometry" user-emacs-directory))

(defun my/save-frame-geometry ()
  (let ((top (frame-parameter (selected-frame) 'top))
        (left (frame-parameter (selected-frame) 'left))
        (width (frame-parameter (selected-frame) 'width))
        (height (frame-parameter (selected-frame) 'height))
        (fullscreen (frame-parameter (selected-frame) 'fullscreen)))
    (if fullscreen
        (with-temp-file my/frame-geometry-file
          (insert
           (format "(add-to-list 'default-frame-alist '(fullscreen . %S))\n" fullscreen)))
      (with-temp-file my/frame-geometry-file
        (insert
         "(dolist (param '("
         (format "(top . %d)\n" top)
         (format "\t(left . %d)\n" left)
         (format "\t(width . %d)\n" width)
         (format "\t(height . %d)))\n" height)
         "(add-to-list 'default-frame-alist param))\n")))))

(defun my/load-frame-geometry ()
  (when (file-readable-p my/frame-geometry-file)
    (load-file my/frame-geometry-file)))

(add-hook 'emacs-startup-hook 'my/load-frame-geometry)
(add-hook 'kill-emacs-hook 'my/save-frame-geometry)

(customize-set-variable
 'evil-disable-insert-state-bindings t)

(setq evil-split-window-below t
      evil-vsplit-window-right nil)

(setq evil-kill-on-visual-paste nil
      evil-ex-substitute-global t
      evil-want-fine-undo t)

(after! evil-snipe
  (setq evil-snipe-scope 'line
        evil-snipe-repeat-scope 'whole-visible))

(defun my/join-line ()
  (interactive)
  (evil-set-marker ?z (point) nil)
  (evil-join (line-end-position) (+ 1 (line-end-position)))
  (evil-goto-mark ?z))

(after! evil
  (map! :nvm "J" 'my/join-line)
  (map! :nvm "'" 'evil-jump-item)
  (map! :nvm "(" 'evil-first-non-blank)
  (map! :nvm ")" 'evil-last-non-blank))

(map! :g "M-c" 'comment-line)

(map! :g "M-q" 'save-buffers-kill-terminal)

(map! :after evil-org
      :map (evil-org-mode-map org-mode-map)
      :nvmi "M-k" 'previous-buffer
      :nvmi "M-j" 'next-buffer)

(map! :nvmi "M-k" 'previous-buffer)
(map! :nvmi "M-j" 'next-buffer)

(map! :g "M-0" 'kill-current-buffer)
(map! :g "M-s" 'save-buffer)
(map! :g "M-i" 'ibuffer)

(map! :i "M-v" 'yank)

(use-package! vertico
  :bind (:map vertico-map
              ("M-k" . vertico-previous)
              ("M-j" . vertico-next)))

(use-package! company
  :bind (:map company-active-map
              ("M-k" . company-select-previous)
              ("M-j" . company-select-next)))

(map! :leader "dj" 'dired-jump)
(map! :leader "do" 'dired-jump-other-window)

(map! :map dired-mode-map
      :n "h" #'dired-up-directory
      :n "l" #'dired-find-alternate-file)

(dolist
    (provider
     '(("Cambridge dictionary" "https://dictionary.cambridge.org/dictionary/english/%s")
       ("Deepl ru" "https://www.deepl.com/translator#en/ru/%s")))
  (add-to-list '+lookup-provider-url-alist provider))

(use-package! google-translate
  :demand t
  :hook (help-mode . +word-wrap-mode)
  :init
  (setq default-input-method "ukrainian-computer")
  :config
  (setq google-translate-default-source-language "en"
        google-translate-default-target-language "uk"
        google-translate-pop-up-buffer-set-focus t
        google-translate-output-destination 'help
        google-translate-backend-method 'curl
        google-translate-show-phonetic t
        google-translate-display-translation-phonetic nil)
  (map! :leader "lb" #'google-translate-buffer)
  (map! :leader "lw" #'google-translate-at-point)
  (map! :leader "lW" #'google-translate-at-point-reverse)
  (map! :leader "ls" #'google-translate-smooth-translate)
  (map! :leader "lq" #'google-translate-query-translate)
  (map! :leader "lQ" #'google-translate-query-translate-reverse))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory (expand-file-name "Documents/OrgFiles/" (getenv "HOME"))
      org-archive-location (expand-file-name ".archive/%s::" org-directory)
      org-agenda-files (list (expand-file-name "Agenda.org" org-directory)))

(setq org-emphasis-regexp-components
      '("-[:space:]('\"{" "-[:space:].,:!?;'\")}\\[‼…⁉⁈" "[:space:]" "." 1))

(after! org
  (setq
   org-ellipsis " …"
   org-startup-folded 'fold
   org-image-actual-width nil
   org-hide-block-startup nil
   org-hide-drawer-startup t
   org-hide-emphasis-markers t
   org-appear-autoemphasis nil
   org-startup-with-inline-images nil
   org-superstar-headline-bullets-list '(9679 9673 9675 10033 10040)
   org-superstar-item-bullet-alist '((43 . 10022) (45 . 10148))))

(defun my/browse-org-directory ()
  (interactive)
  (unless (file-directory-p org-directory)
    (make-directory org-directory t))
  (doom-project-browse org-directory))

(map! :leader :desc "Browse org directory" "fo" 'my/browse-org-directory)

(defun my/org-change-headlines ()
  (dolist (face '((org-level-1 . 1.4) (org-level-2 . 1.3)
                  (org-level-3 . 1.2) (org-level-4 . 1.2)
                  (org-level-5 . 1.2) (org-level-6 . 1.2)
                  (org-level-7 . 1.2) (org-level-8 . 1.2)))
    (set-face-attribute (car face) nil :weight 'heavy :height (cdr face))))

(defun my/org-insert-heading ()
  (evil-open-above 1)
  (evil-normal-state)
  (evil-next-line)
  (evil-append-line 1))

(defun my/org-center-buffer ()
  (setq-local
   visual-fill-column-width 90
   visual-fill-column-center-text t)
  (visual-fill-column-mode 1)
  (visual-line-mode 1))

(after! org
  (add-hook 'org-insert-heading-hook 'my/org-insert-heading)
  (add-hook 'org-mode-hook 'my/org-change-headlines)
  (add-hook 'org-mode-hook 'my/org-center-buffer))

(evil-set-register ?w [?A ?* escape ?^ ?w ?i ?* escape ?j])
(evil-set-register ?e [?A ?/ escape ?^ ?w ?i ?/ escape ?j])

(add-hook! 'doom-after-init-hook
  (add-hook! '+doom-dashboard-functions :append
    (insert "\n" (+doom-dashboard--center +doom-dashboard--width "Pure Evil!"))))

(add-hook! '+doom-dashboard-mode-hook
  (setq-local evil-normal-state-cursor '(hbar . 0))
  (when (featurep 'server-running-p)
    (hl-line-mode 0)))

(remove-hook '+doom-dashboard-functions 'doom-dashboard-widget-footer)

(setq +doom-dashboard-menu-sections nil
      +doom-dashboard-ascii-banner-fn nil)

(customize-set-variable
 'doom-modeline-buffer-file-name-style 'relative-to-project)

(customize-set-variable 'vterm-always-compile-module t)

(setq trash-directory "~/.local/share/Trash/files/"
      magit-delete-by-moving-to-trash t
      delete-by-moving-to-trash t)

(after! recentf
  (add-to-list 'recentf-exclude (expand-file-name ".config/emacs/.local/" (getenv "HOME"))))

(setq tab-width 2)
(setq-default tab-width 2)

(setq standard-indent 2)
(setq-default standard-indent 2)

(add-hook! 'sh-mode-hook
  (setq sh-basic-offset 2
        fish-indent-offset 2))

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
